package app.kolibree.kolibreeapp;

import java.io.IOException;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;

public class FormProfile extends Activity {
	EditText first_name;
	EditText last_name;
	EditText birthday;
	Boolean gender;
	Boolean survey;
	Button apply_button;
	String url="https://test.api.kolibree.com/v1/accounts/request_token/"	;
	String id_account;
	String access_token;
	JSONObject obj;
	String email;
	String password;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_form_profile);
		first_name=(EditText) findViewById(R.id.firstName);
		last_name=(EditText) findViewById(R.id.lastName);
		birthday= (EditText) findViewById(R.id.birthd);
		gender = ((CheckBox) findViewById(R.id.masc)).isChecked();
		survey=((CheckBox) findViewById(R.id.left)).isChecked();
		apply_button=(Button) findViewById(R.id.button1);
		Intent intent=getIntent();
		final String jsontext = intent.getStringExtra("json");
		try {
			obj = new JSONObject(jsontext);
			id_account = obj.getString("id");
			access_token=obj.getString("access_token");
			email=intent.getStringExtra("email");
			password=intent.getStringExtra("password");
		} catch (JSONException e2) {
			// TODO Auto-generated catch block
			e2.printStackTrace();
		}

		apply_button.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v)  {
			     java.lang.System.setProperty("java.net.preferIPv4Stack", "true");
			     java.lang.System.setProperty("java.net.preferIPv6Addresses", "false");
					new Thread(new Runnable(){
						public void run() {
							String url="https://test.api.kolibree.com/v1/accounts/"+id_account+"/profiles/"	;
					        HttpClient client = new DefaultHttpClient();
					        HttpPost post = new HttpPost(url);
				            post.setHeader("Content-type", "application/json");
					        post.addHeader("http-x-client-id","5");
					        post.addHeader("http-x-client-sig","hIYwYic+lnVclk4cJoo7CadN7NV75u/3vVUHHbob+lI=");
					        post.addHeader("http-x-access-token",access_token);
					        String firstname =first_name.getText().toString();
					        String lastname =last_name.getText().toString();
					        String bir =birthday.getText().toString();
					        SimpleDateFormat dmyFormat = new SimpleDateFormat("yyyy-MM-dd");
					         Date myDate = null;
								try {
									myDate = dmyFormat.parse(bir);
								} catch (java.text.ParseException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
					        String gender_text="";
					        String survey_text="";
					        if (gender==true)
					           gender_text ="F";
					        else
					          gender_text="M";
					        if(survey==true)
					          survey_text="L";
					        else
					          survey_text="R";
					        JSONObject jsonParam = new JSONObject();
					        
					       try {
					    	    jsonParam.put("first_name",firstname );
					    	    jsonParam.put("last_name",lastname );
					    	    jsonParam.put("birthday",dmyFormat.format(myDate) );
					    	    jsonParam.put("gender",gender_text );
					    	    jsonParam.put("survey_handedness",survey_text );
							    post.setEntity(new StringEntity(jsonParam.toString(), "UTF8"));
							    HttpResponse response = client.execute(post);

							   } catch (ClientProtocolException e)
							       {
								      Log.e( "HttpManager", "ClientProtocolException thrown" + e );
								    
							       }
							   	  catch (SocketException e)
								  {
								   Log.e( "SocketException", "SocketException thrown" + e );
								  } catch (ParseException e)
								  {
									e.printStackTrace();
								  } catch (IOException e)
								  {
									// TODO Auto-generated catch block
									e.printStackTrace();
								  } catch (JSONException e)
								  {
									// TODO Auto-generated catch block
									e.printStackTrace();
								  }
							}} ).start();
	                Toast.makeText(getBaseContext(), "Profile Added Successfully,Connect again please", Toast.LENGTH_SHORT).show();
	                Intent inten=new Intent(getApplicationContext(),MainActivity.class);
	                startActivity(inten);

				}
		});
	}
}
