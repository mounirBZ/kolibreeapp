package app.kolibree.kolibreeapp;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class MainActivity extends Activity {
	Button create_account;
	EditText email;
	EditText password;
	String _email="";
	String _password="";
	String url="https://test.api.kolibree.com/v1/accounts/request_token/"	;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		create_account=(Button) findViewById(R.id.button);
		email=(EditText) findViewById(R.id.email);
		password=(EditText)findViewById(R.id.password);
		 
		create_account.setOnClickListener(new OnClickListener() {	
			@Override
			public void onClick(View arg0) {
				new Thread(new Runnable(){
					
					public void run(){
				
					        HttpClient client = new DefaultHttpClient();
					        HttpPost post = new HttpPost(url);
					        post.addHeader("http-x-client-id","5");
					        post.addHeader("http-x-client-sig","ipcwp9Sh0ZfNc9CfBKlZTvdT/mSVArCuakNtc2Tt7TQ=");
					        List<NameValuePair> nvp= new ArrayList<NameValuePair>();
					        _email=email.getText().toString();
					        _password=password.getText().toString();
					        nvp.add(new BasicNameValuePair("email",_email));
					        nvp.add(new BasicNameValuePair("password",_password));
					        try {
								post.setEntity(new UrlEncodedFormEntity(nvp));
							    HttpResponse response = null;
								response = client.execute(post);
								String responseBody = EntityUtils.toString(response.getEntity());
								JSONObject myObject = new JSONObject(responseBody);
								Intent myIntent = new Intent(getBaseContext(), Account.class);
								myIntent.putExtra("json", myObject.toString());

								startActivity(myIntent);
							   } catch (ClientProtocolException e)
							     	{
								   		Log.e( "HttpManager", "ClientProtocolException thrown" + e );
							     	}
								catch (IOException e)
									{
										Log.e( "HttpManager", "IOException thrown" + e );
									} catch (JSONException e) {
										e.printStackTrace();
									}
							}} ).start();
							
						}
					});
				
	}	
}
