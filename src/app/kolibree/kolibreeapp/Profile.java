package app.kolibree.kolibreeapp;

public class Profile {
	private String first_name;
	private String last_name;
	private String gender;
	public Profile(){
		
	}
	public Profile(String first_name,String last_name,String gender){
		this.first_name=first_name;
		this.last_name=last_name;
		this.gender=gender;
	}
	
	public String toString(){
		return "fist name: "+this.first_name + "\nlast name:  " + this.last_name + "\n gender : " + this.gender ;
	}
	public String getFirst_name() {
		return first_name;
	}
	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}
	public String getLast_name() {
		return last_name;
	}
	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	
}
