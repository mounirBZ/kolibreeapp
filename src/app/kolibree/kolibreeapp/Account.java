package app.kolibree.kolibreeapp;


import java.util.ArrayList;
import java.util.HashMap;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.app.Activity;
import android.content.Intent;

public class Account extends Activity {
	ListView listProfiles;
	Button addProfile;
	String id_account="";
	String access_token="";
	ArrayAdapter<String> adapter;
	JSONObject obj;
	JSONArray jsonarray;
	HashMap<String,Profile> nameProfile;
	ArrayList<String> array;
	String email;
	String password;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_account);
		listProfiles = (ListView) findViewById(R.id.listView1);
		addProfile= (Button) findViewById(R.id.button);
		Intent intent = getIntent();
		final String jsontext = intent.getStringExtra("json");
		jsonarray= new JSONArray();
		nameProfile = new HashMap<String,Profile>();
		array = new ArrayList<String>();
		
		try {
			
			obj = new JSONObject(jsontext);
			id_account=obj.getString("id");
			access_token=obj.getString("access_token");
			jsonarray=obj.getJSONArray("profiles");
			createProfile(obj,jsonarray,nameProfile,array);
	        adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1, array);
	        listProfiles.setAdapter(adapter);
	        listProfiles.setOnItemClickListener(new OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View view, int position,
	                    long id) {
	                
	                String item = ((TextView)view).getText().toString();
	                Toast.makeText(getBaseContext(), nameProfile.get(item).toString(), Toast.LENGTH_LONG).show();
	            }
	        });
		} catch (JSONException e2) {
			e2.printStackTrace();
		} 
		
		
		addProfile.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				Intent newProfile= new Intent(getBaseContext(), FormProfile.class);
				newProfile.putExtra("json", jsontext);
				newProfile.putExtra("email", email);
				newProfile.putExtra("password", password);
				startActivity(newProfile);
			}
		});
		
	}
	public void createProfile(JSONObject jsonobject,JSONArray jsonarray,HashMap<String,Profile> m,ArrayList<String> arraylist) throws JSONException{
		for (int j = 0; j <jsonarray.length(); j++){
		jsonobject =jsonarray.getJSONObject(j);
		String  first_name = jsonobject.getString("first_name");
		String last_name=jsonobject.getString("last_name");
		String gender=jsonobject.getString("gender");
		Profile profile= new Profile(first_name,last_name,gender);
		m.put(profile.getFirst_name(), profile);
		arraylist.add(profile.getFirst_name());
	}	
	}

}
